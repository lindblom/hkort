//
//  ProgramTests.m
//  ProgramTests
//
//  Created by Christopher Lindblom on 2011-12-20.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ProgramTests.h"

@implementation ProgramTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in ProgramTests");
}

@end
